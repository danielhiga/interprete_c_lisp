;Interprete de c en lisp (clase 29/08/18)

;Funcion Run:
;arma la memoria y chequea que este main
; SOLO CREAR LA VARIABLE EN 0
(Defun Run (Prog &optional (Entrada nil) (Memoria nil))
	(IF (null Prog) NIL
		(IF (eq (caar Prog) 'int)
		    (IF (null (cdar Prog))
			   (Run (cdr Prog) Entrada (Agregar_Mem (cadar Prog) Memoria))
			   (Run (cdr Prog) Entrada (asignar (nth 1 (car prog)) (nth 3 (car prog)) (Agregar_Mem (cadar Prog) Memoria)))

			)
			(IF (eq (caar Prog) 'main)
				(Ejecutar (nth 1 (car Prog)) Entrada Memoria)
				'ERROR
			)
		)
	)
)


(Defun Ejecutar (Prog ent Mem &optional (sal nil)) ;sal=salida ent=entrada
	(IF (null Prog) (reverse sal)
		(cond
			((eq (caar  Prog) 'cin) (Ejecutar (cdr Prog) (cdr ent) (Asignar (nth 1 (car Prog)) (car ent) Mem) sal))
			((eq (caar  Prog) 'scanf) (Ejecutar (cons (cons 'cin (cdar Prog)) (cdr Prog)) ent Mem sal))
			((eq (caar Prog) 'printf) (Ejecutar (cons (cons 'cout (cdar Prog)) (cdr Prog)) ent Mem sal))
			((eq (caar Prog) 'cout) (Ejecutar (cdr Prog) ent Mem (cons (valor(cdar Prog) Mem) sal)))
			((eq (caar Prog) 'debug) (print mem) sal)
			((eq (cadar Prog) '=) (Ejecutar (cdr Prog) ent (Asignar (caar Prog) (valor (cddar Prog) Mem) Mem) sal))
			((pertenece (cadar Prog) '(+= -= /= *= ++ --)) (Ejecutar (cons (Convertir (car Prog)) (cdr Prog)) ent Mem sal))
			((pertenece (caar Prog) '(++ --)) (Ejecutar(cons (reverse (car Prog)) (cdr Prog)) ent Mem sal))
			((eq (caar Prog) 'if) (if (eq (valor (nth 1 (car Prog)) Mem) 0) 
									(if (eq (length (car Prog)) 5) 
										(Ejecutar (append (nth 4 (car Prog)) (cdr Prog)) ent Mem sal) (Ejecutar (cdr Prog) ent Mem sal))
										(Ejecutar (append (nth 2 (car Prog)) (cdr Prog)) ent Mem sal)))
			((eq (caar Prog) 'while) (if (eq (valor (nth 1 (car Prog)) Mem) 0) (Ejecutar(cdr Prog) ent Mem sal) (Ejecutar (append(nth 2 (car Prog)) Prog) ent Mem sal)))
		)
	)
)



;funcion 'valor'
;precedencia de simbolos en c
;lo ideal seria armar 3 listas para separar 1 para operadores, otra para operandos y otra para la expresion que recibe
(Defun valor (Expr Mem &optional (operadores nil) (operandos nil));completar 

	(if (and (atom Expr) (not (null Expr)))
			(if (numberp Expr) Expr
				(Buscar Expr Mem)
			)
			
			(if (null Expr) 
				(if (null operadores) (car operandos)
					(valor Expr Mem (cdr operadores) (cons (operar(car operadores) (cadr operandos) (car operandos)) (cddr operandos)))
				)
				(if (es_operador (car Expr))
					(if (null operadores)
						(valor (cdr Expr) Mem (cons (car Expr) operadores) operandos)
						(if (> (peso(car Expr)) (peso (car operadores)))
							(valor (cdr Expr) Mem (cons (car Expr) operadores) operandos)
							(valor Expr Mem (cdr operadores) (cons (operar (car operadores) (cadr operandos) (car operandos)) (cddr operandos)))
						)
					)
				    (valor (cdr Expr) Mem operadores (cons (valor (car Expr) Mem) operandos));si no es operador
				)
			)
	)

)




;Funcion pertenece devuelve t si el atomo esta en la lista
(Defun pertenece (a l)
   (numberp (position a l))
)

;Funcion Buscar ejemplo: (buscar 'a '(x 3 a o))-->3
(Defun Buscar (a l)
 (nth (+ (position a l) 1) l)
)


;Ejemplo: (Asignar 'x '1 '(x 3 a 0 b 0))--> (x 1 a 0 b 0)
(Defun asignar(var val l)
  (if (null l)
    (error "no se encontro")
    (if (eq var (car l))
      (append (list (car l) val) (cddr l))
      (append (list (car l) (cadr l)) (asignar var val (cddr l)))
    )
  )
)


;Funcion Convertir Ejemplo: (Convertir '(a + = 8))--> (a = a + 8)

(Defun Convertir (l)
  (cond
	; unario
	((eq '++ (cadr L)) (list (car L) '= (car L) '+ '1))
	((eq '-- (cadr L)) (list (car L) '= (car L) '- '1))
	
	
	; binario
	((eq '+= (cadr L)) (list (car L) '= (car L) '+ (caddr L)))
	((eq '-= (cadr L)) (list (car L) '= (car L) '- (caddr L)))
	((eq '*= (cadr L)) (list (car L) '= (car L) '* (caddr L)))
	((eq '/= (cadr L)) (list (car L) '= (car L) '/ (caddr L)))
	(error "convertir ")
	)
)




;Funcion operar
;devuelve un atomo:tiene que operar hacer la cuenta, y devolver el resultado
(Defun operar (operador op1 op2)
(cond ((pertenece operador '(sqrt)); raiz cuadrada
   (sqrt op2))
   ((pertenece operador '(+ - *)) (eval (list operador op1 op2)))
   ((pertenece operador '(/))
   ; Si es division o resto verifico que no divida por 0
   (if (eq op1 0)
      (error "no se puede dividir por 0")
      (eval (list operador op1 op2))
   ))
   ; Operaciones Relacionales
   ; Solo opera numeros
   ((pertenece operador '(< <= > >=)) ; mayor - menor
    (if (and (numberp op1) (numberp op2))
	  (if (eval (list operador op1 op2))
	    1
	    0
	  )
	  (error "operandos deben ser numericos")
    ))
	((pertenece operador '(!=))
	(if (not (eq op1 op2)) 1
	0
	)
	)
	((pertenece operador '(==))
	(if (eq op1 op2) 1
	0
	))
	)
)


;Funcion peso
;buscar tabla para precedencia de operadores, y le damos un valor
;Ejemplo (peso '+)--> 8
(Defun peso (a)
   (position a (lista_operadores))
)


;Funcion Agregar_Mem
;Ejemplo:(Agregar_Mem '(x = 3 a b) 'NIL)--> (x 3 a b) usar eq
;no se tiene en cuenta mayusculas o minisculas
(Defun agregar_mem (a l)
    (append (list a 0) l)
)


;ver si L1 que es la memoria puede ser que tenga algo de antes ejemplo '(f g)
(Defun es_operador (a)
  (not (eq nil (find a (lista_operadores))))
)

;-----------------------
;start extra functions
(defun lista_operadores()
  (reverse '(++ -- ! ~ + - * / % sqrt < <= > >= == != & ^ \| && ||))
)
;end extra functions
;-----------------------
